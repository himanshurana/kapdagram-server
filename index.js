require("dotenv").config();
var express = require("express");
var app = express();
const cors = require("cors");
app.use(cors({credentials: true, origin: 'http://localhost:3000'}));

var mongoose = require('mongoose');
// const cookieSession = require('cookie-session');

// var cookieParser = require('cookie-parser');

const passport = require('passport');

const bodyParser = require("body-parser");


require("./models")
require('./services/passport');

const errorHandler = require("./handlers/error");
const authRoutes = require("./routes/auth");
const productsRoutes = require("./routes/products");
const ReviewRoutes = require("./routes/reviewRoutes");
const addressesRoutes = require("./routes/address");
const cartRoutes = require("./routes/cart");
const { loginRequired, ensureCorrectUser } = require("./middleware/auth");
const categoriesRoutes = require("./routes/categoriesRoutes");
const ordersRoutes = require("./routes/ordersRoutes");

const keys = require('./config/keys')
const db = require("./models");

const PORT = 8081;

var Cart = require('./models/cart');

app.use(bodyParser.json());

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);


// --->>> SET UP SESSIONS <<<---
var database = mongoose.connection;
database.on('error', console.error.bind(console, '# MongoDB - connection error: '));
app.use(session({
  secret: 'mySecretString',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 1000 * 60 * 60 * 24 * 2 }, // 2 days in milliseconds  
  store: new MongoStore({ mongooseConnection: database, ttl: 2 * 24 * 60 * 60 })
  //ttl: 2 days * 24 hours * 60 minutes * 60 seconds 
}))
//--->>> END SESSION SET UP <<<---



// app.use(cookieParser())
//=============== Cookie Session ======================//

// tell express to use cookie
// app.use(
//   cookieSession({
//     maxAge: 30 * 24 * 60 * 60 * 1000,
//     keys: [keys.cookieKey]
//   })
// );

// tell passport. it should use cookies to handle authentication
app.use(passport.initialize());
app.use(passport.session());;

app.use("/api/user", addressesRoutes);

app.use("/", authRoutes, productsRoutes, categoriesRoutes, cartRoutes);
// app.use("/", cartRoutes);
app.use("/api/user", ordersRoutes)
app.use(
  "/api/users/:id/reviews",
  // loginRequired,
  // ensureCorrectUser,
  ReviewRoutes
);

app.use(
  "/api/users/:id/addresses",
  loginRequired,
  ensureCorrectUser,
  addressesRoutes
);

app.use('/uploads', express.static('uploads'));

app.get('/shopping-cart', function(req, res, next) {
  if (!req.session.cart) {
      return res.send({products: null});
  }
   var cart = new Cart(req.session.cart);
   res.send({products: cart.generateArray(), totalPrice: cart.totalPrice});
});

app.get("/api/reviews", async function (req, res, next) {
  try {
    let reviews = await db.Review.find()
      .sort({ createdAt: "desc" })
      .populate("user", {
        displayName: true
      });
      console.log(reviews)
    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
});

app.get("/api/addresses", async function (req, res, next) {
  try {
    let addresses = await db.Address.find()
      .sort({ createdAt: "desc" })
      .populate("user", {
        email: true,
        profileImageUrl: true
      });
    return res.status(200).json(addresses);
  } catch (err) {
    return next(err);
  }
});


//////////////////////////////////////////////////////////
app.use(function (req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(errorHandler);

app.listen(PORT, function () {
  console.log(`Server is starting on port ${PORT}`);
});







// // --->>> GET BOOKS IMAGES API <<<-----  
// app.get('/images', function (req, res) {
//   const imgFolder = __dirname + '/public/images/';
//   // REQUIRE FILE SYSTEM 
//   const fs = require('fs');
//   //READ ALL FILES IN THE DIRECTORY 
//   fs.readdir(imgFolder, function (err, files) {
//     if (err) {
//       return console.error(err);
//     }
//     //CREATE AN EMPTY ARRAY 
//     const filesArr = [];
//     // ITERATE ALL IMAGES IN THE DIRECTORY AND ADD TO THE THE ARRAY     
//     files.forEach(function (file) {
//       filesArr.push({ name: file });
//     });
//     // SEND THE JSON RESPONSE WITH THE ARARY  
//     res.json(filesArr);
//   })
// })
// // END APIs

