const db = require("../models");

exports.createAddress = async function (req, res, next) {
    try {
        let address = await db.Address.create({
            address: req.body.address,
            user: req.params.id
        });
        let foundUser = await db.User.findById(req.params.id);
        console.log(foundUser)
        await foundUser.addresses.push(address.id);
        await foundUser.save();
        let foundAddress = await db.Address.findById(address._id).populate("user", {
            email: true,
            profileImageUrl: true
        });
        return res.status(200).json(foundAddress);
    } catch (err) {
        return next(err);
    }
};

// GET - /api/users/:id/messages/:message_id
exports.getAddress = async function (req, res, next) {
    try {
        let address = await db.Address.find(req.params.address_id);
        return res.status(200).json(address);
    } catch (err) {
        return next(err);
    }
};

// DELETE /api/users/:id/messages/:message_id
exports.deleteAddress = async function (req, res, next) {
    try {
        let foundAddress = await db.Address.findById(req.params.address_id);
        await foundAddress.remove();

        return res.status(200).json(foundAddress);
    } catch (err) {
        return next(err);
    }
};
