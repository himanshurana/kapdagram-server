const models = require("../models");
var fs = require('fs');

////////////////// Admin Panel /////////////////////////////
const callbacks = {
  postProducts:
    async function (req, res, next) {
      //image file url
      var imageURL = req.protocol + '://' + req.get('host') + '/' + req.file.path;
      try {
        let product = await new models.Product({
          title: req.body.title,
          description: req.body.description,
          img: imageURL,
          prices: {
            sell_price: req.body.sell_price,
            mrp: req.body.mrp,
            discount: req.body.discount
          },
          categorie: { _id: req.body.categorie },
          reviews: { _id: req.body.review }
        }).save();

        //find user who belong to this message
        let foundCategorie = await models.Categorie.findById(req.body.categorie);
        console.log('foundCategorie', foundCategorie)
        //push this message into found user 
        foundCategorie.products.push(product.id);
        // console.log('foundCategorie', foundCategorie)
        //then save the updated user
        await foundCategorie.save();

        return res.status(200).json(product);
      } catch (err) {
        return next(err);
      }
    },
  getProducts:
    async function (req, res, next) {
      try {
        // fs.unlink('./uploads/40592159_2156635587744845_5265889535669043200_n.jpg', function (err) {
        //   if (err) throw err;
        //   // if no error, file has been deleted successfully
        //   console.log('File deleted!');
        // });
        let products = await models.Product.find();
        return res.status(200).json(products);
      } catch (err) {
        return next(err);
      }
    },
  deleteProducts:
    async function (req, res, next) {
      let query = { _id: req.body._id };
      try {
        let products = await models.Product.remove(query);

        //------ Also delete this product id from Categories Collection --
        // Create a copy of the current array of Categories
        const Categories = await models.Categorie.find();
        // console.log('length', Categories.length)
        for (let i = 0; i < Categories.length; i++) {
          // console.log("loop")
          // Determine at which index in products array is the product id to be deleted
          const indexToDelete = Categories[i].products.findIndex(
            function (id) {
              // console.log('id', id)
              return id == req.body._id;
            }
          )
          // console.log('indexToDelete', indexToDelete)
          // use slice to remove the product id at the specified index
          if (indexToDelete !== -1) {   // findIndex function return -1 if it doesn't find comparing value
            // console.log("before", Categories)
            let productsArrAfterDelete = [...Categories[i].products.slice(0, indexToDelete), ...Categories[i].products.slice(indexToDelete + 1)];
            // console.log('productsArrAfterDelete', productsArrAfterDelete)
            //update the products arr in Categories Collection document 
            Categories[i].products = productsArrAfterDelete;
            // console.log("after", Categories)
            // Update the Categories Collection
            await models.Categorie.create(Categories)
          }
          //------ End --
        }
        return res.status(200).json(products);
      } catch (err) {
        return next(err);
      }
    },
  updateProducts:
    async function (req, res) {
      var product = req.body;
      // console.log(product)
      var query = req.params._id;
      // if the field doesn't exist $set will set a new field  
      var update = {
        '$set': {
          title: product.title,
          description: product.description,
          image: product.image,
          price: product.price
        }
      };
      // When true returns the updated document    
      var options = { new: true };

      models.Product.findOneAndUpdate(query, update, options, function (err, products) {
        if (err) {
          throw err;
        }
        res.json(products);
      })
    }
}

module.exports = callbacks;