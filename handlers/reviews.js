const models = require("../models");

//api/users/:id/messages
exports.creatReview = async function(req, res, next) {
  try {
    //create a message
    let review = await models.Review.create({
      text: req.body.text,
      user: req.params.id,
      product: req.body.product
    });
    //find user who belong to this message
    let foundUser = await models.User.findById(req.params.id);
    let foundProduct = await models.Product.findById(req.body.product);
    //push this message into found user 
    foundUser.reviews.push(review.id);
    foundProduct.reviews.push(review.id);
    //then save the updated user
    await foundUser.save();
    await foundProduct.save();
    //then populate the user to this message
    let foundReview = await models.Review.findById(review._id).populate("user", {
      username: true,
      email: true,
      profileImageUrl: true
    });
    //and send back as a response
    return res.status(200).json(foundReview);
  } catch (err) {
    return next(err);
  }
};

// GET - /api/users/:id/messages/:message_id
exports.getReview = async function(req, res, next) {
  try {
    let review = await models.Review.find(req.params.review_id);
    return res.status(200).json(review);
  } catch (err) {
    return next(err);
  }
};

// DELETE /api/users/:id/messages/:message_id
exports.deleteReview = async function(req, res, next) {
  try {
    let foundReview = await models.Review.findById(req.params.review_id);
    console.log(req.params.review_id)
    await foundReview.remove();

    //------ Also delete this product id from Categories Collection --
        // Create a copy of the current array of Categories
        const Product = await models.Product.find();
        // console.log('length', Categories.length)
        for (let i = 0; i < Product.length; i++) {
          // console.log("loop")
          // Determine at which index in products array is the product id to be deleted
          const indexToDelete = Product[i].reviews.findIndex(
            function (id) {
              // console.log('id', id)
              return id == req.params.review_id;
            }
          )
          // console.log('indexToDelete', indexToDelete)
          // use slice to remove the product id at the specified index
          if (indexToDelete !== -1) {   // findIndex function return -1 if it doesn't find comparing value
            // console.log("before", Categories)
            let reviewsArrAfterDelete = [...Product[i].reviews.slice(0, indexToDelete), ...Product[i].reviews.slice(indexToDelete + 1)];
            // console.log('productsArrAfterDelete', productsArrAfterDelete)
            //update the products arr in Categories Collection document 
            Product[i].reviews = reviewsArrAfterDelete;
            // console.log("after", Categories)
            // Update the Categories Collection
            await models.Product.create(Product)
          }
          //------ End --
        }

    return res.status(200).json(foundReview);
  } catch (err) {
    return next(err);
  }
};
