const models = require("../models");

////////////////// Admin Panel /////////////////////////////
const callbacks = {
    postStaffPickProduct:
        async function (req, res, next) {
            // console.log(req.body.categories._id)
            try {
                let staffPickProduct = await models.Products.create(req.body);

                return res.status(200).json(staffPickProduct);
            } catch (err) {
                return next(err);
            }
        },
    getStaffPickProducts:
        async function (req, res) {
            try {
                let staffPickProducts = await models.StaffPickProducts.find();
                return res.status(200).json(staffPickProducts);
            } catch (err) {
                return next(err);
            }
        },
    deleteStaffPickProduct:
        async function (req, res, next) {
            let query = { _id: req.body._id };
            try {
                let staffPickProducts = await models.StaffPickProducts.remove(query);
                return res.status(200).json(staffPickProducts);
            } catch (err) {
                return next(err);
            }
        },
    updateStaffPickProduct:
        async function (req, res) {
            var staffPickProduct = req.body;
            // console.log(product)
            var query = req.params._id;
            // if the field doesn't exist $set will set a new field  
            var update = {
                '$set': {
                    title: product.title,
                    description: product.description,
                    image: product.image,
                    price: product.price
                }
            };
            // When true returns the updated document    
            var options = { new: true };

            models.StaffPickProducts.findOneAndUpdate(query, update, options, function (err, updatedStaffPickProduct) {
                if (err) {
                    throw err;
                }
                res.json(updatedStaffPickProduct);
            })
        }
}

module.exports = callbacks;