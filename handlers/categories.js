const models = require('../models');

exports.createCategorie = async function (req, res, next) {
    try {
        let categorie = await models.Categorie.create(req.body);
        return res.status(200).json(categorie);
    }
    catch (err) {
        return next(err);
    }
}

exports.getCategories = async function (req, res, next) {
    try {
        let categories = await models.Categorie.find();
        return res.status(200).json(categories);
    }
    catch (err) {
        return next(err);
    }
}


exports.deleteCategorie = async function (req, res, next) {
    let query = { _id: req.body._id };
    try {
        let categorie = await models.Categorie.remove(query);
        return res.status(200).json(categorie);
    } catch (err) {
        return next(err)
    }
}
