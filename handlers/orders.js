const models = require("../models");

////////////////// Admin Panel /////////////////////////////
const callbacks = {
    postOrders:
        async function (req, res, next) {
            // var cart = new Cart(req.session.cart);
            try {
                let orders = await models.Order.create({
                    user: req.user,
                    shipping: req.body.shipping,
                    payment: req.body.payment,
                    // cart: cart
                });
                req.session.cart = null;
                return res.status(200).json(orders);
            } catch (err) {
                return next(err);
            }
        },
    getOrders:
        async function (req, res) {
            try {
                let orders = await models.Order.find();
                return res.status(200).json(orders);
            } catch (err) {
                return next(err);
            }
        },
    deleteOrders:
        async function (req, res, next) {
            let query = { _id: req.body._id };
            try {
                let products = await models.Products.remove(query);

                //------ Also delete this product id from Categories Collection --
                // Create a copy of the current array of Categories
                const Categories = await models.Categories.find();
                // console.log('length', Categories.length)
                for (let i = 0; i < Categories.length; i++) {
                    // console.log("loop")
                    // Determine at which index in products array is the product id to be deleted
                    const indexToDelete = Categories[i].products.findIndex(
                        function (id) {
                            // console.log('id', id)
                            return id == req.body._id;
                        }
                    )
                    // console.log('indexToDelete', indexToDelete)
                    // use slice to remove the product id at the specified index
                    if (indexToDelete !== -1) {   // findIndex function return -1 if it doesn't find comparing value
                        // console.log("before", Categories)
                        let productsArrAfterDelete = [...Categories[i].products.slice(0, indexToDelete), ...Categories[i].products.slice(indexToDelete + 1)];
                        // console.log('productsArrAfterDelete', productsArrAfterDelete)
                        //update the products arr in Categories Collection document 
                        Categories[i].products = productsArrAfterDelete;
                        // console.log("after", Categories)
                        // Update the Categories Collection
                        await models.Categories.create(Categories)
                    }
                    //------ End --
                }
                return res.status(200).json(products);
            } catch (err) {
                return next(err);
            }
        }
}

module.exports = callbacks;