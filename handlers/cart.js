const models = require('../models');
var Cart = require('../models/cart');
// POST CART
exports.createCart = function (req, res, next) {
  var productId = req.params.id;
  var cart = new Cart(req.session.cart ? req.session.cart : {});
  // models.Product.findById('5bca301b69469033783033b4').then(res=>{console.log(res)})

  models.Product.findById(productId, function (err, product) {
    if (err) {
      return res.redirect('/');
    }
    cart.add(product, product.id);
    req.session.cart = cart;
    // console.log(req.session);
    res.send(req.session.cart)
    // res.redirect('/');
  });
};

// GET CART 
exports.getCart = async function (req, res) {
  if (!req.session.cart) {
    return res.send({ products: null });
  }
  var cart = new Cart(req.session.cart);
  const response = { products: cart.generateArray(), totalQty: cart.totalQty, totalPrice: cart.totalPrice };
  res.send(response);
}

exports.reduce = function (req, res){
  var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart : {});

    cart.reduceByOne(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart');
}











































// //api/users/:id/messages
// exports.createCart = async function(req, res, next) {
//   try {
//     //create a message
//     let product = await db.Message.create({
//       text: req.body.text,
//       user: req.params.id
//     });
//     //find user who belong to this message
//     let foundUser = await db.User.findById(req.params.id);
//     //push this message into found user 
//     foundUser.messages.push(message.id);
//     //then save the updated user
//     await foundUser.save();
//     //then populate the user to this message
//     let foundMessage = await db.Message.findById(message._id).populate("user", {
//       username: true,
//       email: true,
//       profileImageUrl: true
//     });
//     //and send back as a response
//     return res.status(200).json(foundMessage);
//   } catch (err) {
//     return next(err);
//   }
// };

// // GET - /api/users/:id/messages/:message_id
// exports.getProduct = async function(req, res, next) {
//   try {
//     let message = await db.Message.find(req.params.message_id);
//     return res.status(200).json(message);
//   } catch (err) {
//     return next(err);
//   }
// };

// // DELETE /api/users/:id/messages/:message_id
// exports.deleteProduct = async function(req, res, next) {
//   try {
//     let foundMessage = await db.Message.findById(req.params.message_id);
//     await foundMessage.remove();

//     return res.status(200).json(foundMessage);
//   } catch (err) {
//     return next(err);
//   }
// };
