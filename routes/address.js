const express = require("express");
const router = express.Router({ mergeParams: true });

const {
    createAddress,
    getAddress,
    deleteAddress
} = require("../handlers/address");

// prefix - /api/users/:id/messages
router.route("/:id/address").post(createAddress);

// prefix - /api/users/:id/messages/:message_id
router
    .route("/:id/addresses")
    .get(getAddress)
    .delete(deleteAddress);

module.exports = router;
