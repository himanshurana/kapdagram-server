const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../handlers/orders");


router.route('/:id/orders')
    .post(callbacks.postOrders)
    .get(callbacks.getOrders)
    .delete(callbacks.deleteOrders)
    
module.exports = router;