const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../handlers/products");


router.route('/api/staff_picks')
    .post(callbacks.postStaffPickProduct)
    .get(callbacks.getStaffPickProducts)
    .delete(callbacks.deleteStaffPickProduct)
    .put(callbacks.updateStaffPickProduct)

module.exports = router;
