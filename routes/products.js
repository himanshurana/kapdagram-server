const express = require("express");
const router = express.Router({ mergeParams: true });
const callbacks = require("../handlers/products");
const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
});

const upload = multer({ storage: storage });


router.route('/api/products')
    .post(
        upload.single('img')
        , callbacks.postProducts)
    .get(callbacks.getProducts)
    .delete(callbacks.deleteProducts)
    .put(callbacks.updateProducts)

module.exports = router;
