const express = require("express");
const router = express.Router({ mergeParams: true });

const { createCategorie, getCategories, deleteCategorie } = require("../handlers/categories");

router.route('/api/categories')
.post(createCategorie)
.get(getCategories)
.delete(deleteCategorie)

module.exports = router;