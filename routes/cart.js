const express = require("express");
const router = express.Router({ mergeParams: true });
var Cart = require('../models/cart');
const models = require('../models')

const {
  createCart,
  getCart,
  reduce,
  deleteCart
} = require("../handlers/Cart");

router.post('/add-to-cart/:id', createCart);
router.route("/cart").get(getCart);
router.get('/reduce/:id', reduce);
module.exports = router;
