const mongoose = require("mongoose");
const User = require("./user");

const addressSchema = new mongoose.Schema(
    {
        address: {
            type: String,
        },
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        }
    },
    {
        timestamps: true
    }
);

addressSchema.pre("remove", async function (next) {    // right before "remove" or "delete" the address
    try {
        // find a user
        let user = await User.findById(this.user);  // "this" refer to that(addressSchema) specific document
        // remove the id of the message from their messages list from user collection
        user.addresss.remove(this.id);
        // save that user
        await user.save();
        // return next
        return next();
    } catch (err) {
        return next(err);
    }
});

//make an Address Collection(Model) in db
const Address = mongoose.model("Address", addressSchema);
module.exports = Address;
