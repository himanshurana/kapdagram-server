const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: String,
    img: { type: String },
    prices: {
        sell_price: { type: Number, required: true },
        mrp: { type: Number },
        discount: { type: Number }
    },
    categorie: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "categories",
            required: true
        }
    ],
    reviews: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Review"
        }
    ]
});

const Product = mongoose.model("Products", productSchema);

module.exports = Product;