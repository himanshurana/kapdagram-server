const mongoose = require("mongoose");
const bcrypt = require("bcrypt"); // bcrypt library is going to be used for password hashing.

const userSchema = new mongoose.Schema({
  googleId: { type: String },
  email: {type: String},
  displayName: {type: String},
  reviews: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Review"
    }
  ],
  addresses: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Address"
    }
  ]
});

const User = mongoose.model("User", userSchema);

module.exports = User;
