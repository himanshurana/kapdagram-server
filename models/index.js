const mongoose = require("mongoose");
mongoose.set("debug", true);  // It is important if we want to show real mongo query in terminal
mongoose.Promise = Promise;   // We will use native es2015 "promises" library so that we don't have to use callback pattern. this line of code is very important because
// we're going to be using es2017 async functions to build this application and async functions return "promises". so 
// we really want to make sure that our "mongoose methods" are returning "promises" so everything works.
mongoose.connect("mongodb://rana:rana123@ds221003.mlab.com:21003/kapdagram", {
  keepAlive: true
});

const models = {
  Product: require("./product"),
  User: require("./user"),
  Review: require("./review"),
  Address: require("./address"),
  books: require("./books"),
  Categorie: require("./categorie"),
  Order: require("./Order")
}
// module.exports.Clothes = require("./products");
// module.esxports.Crockeries = require("./products");

module.exports = models;