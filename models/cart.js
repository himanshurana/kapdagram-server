module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.totalQty = oldCart.totalQty || 0;
    this.totalPrice = oldCart.totalPrice || 0;

    this.add = function (item, id) {
        var storedItem = this.items[id];
        if (!storedItem) {
            storedItem = this.items[id] = { item: item, qty: 0, price: 0 };
        }
        console.log(storedItem)
        storedItem.qty++;
        storedItem.price = storedItem.item.prices.sell_price * storedItem.qty;
        this.totalQty++;
        this.totalPrice += storedItem.item.prices.sell_price;
    };

    this.reduceByOne = function (id) {
        console.log(this.items)
        this.items[id].qty--;
        this.items[id].price -= this.items[id].item.prices.sell_price;
        this.totalQty--;
        this.totalPrice -= this.items[id].item.prices.sell_price;

        if (this.items[id].qty <= 0) {
            delete this.items[id];
        }
    };

    this.removeItem = function (id) {
        this.totalQty -= this.items[id].qty;
        this.totalPrice -= this.items[id].price;
        delete this.items[id];
    };

    this.generateArray = function () {
        var arr = [];
        for (var id in this.items) {
            arr.push(this.items[id]);
        }
        return arr;
    };
};

// const mongoose = require("mongoose");

// const cartSchema = new mongoose.Schema({
//     title: {
//         type: String,
//         required: true,
//     },
//     description: String,
//     img: { type: String, required: true },
//     prices: {
//         sell_price: { type: Number, required: true },
//         mrp: { type: Number },
//         discount: { type: Number }
//     }
// });

// const Cart = mongoose.model("Cart", cartSchema);

// module.exports = Cart;