const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    shipping: {
        customer: { type: String },
        address: { type: String },
        delivery_notes: { type: String },

        tracking: {
            company: { type: String },
            tracking_number: { type: String },
            status: { type: String }
        },
    },
    payment: {
        method: { type: String },
        transaction_id: { type: String }
    },
    cart: { type: Object, required: true }
});

const Order = mongoose.model("Order", orderSchema);

module.exports = Order;