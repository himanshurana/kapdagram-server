const mongoose = require("mongoose");

const categorieSchema = new mongoose.Schema({
    categorie: {
        type: String,
        required: true,
    },
    products: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Products"
        }
    ]
});

const Categorie = mongoose.model("categories", categorieSchema);

module.exports = Categorie;